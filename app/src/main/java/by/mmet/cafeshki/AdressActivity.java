package by.mmet.cafeshki;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
//import android.support.design.widget.TextInputLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.model.LatLng;

import android.support.design.widget.TextInputLayout;

import by.mmet.cafeshki.DataBase.DBHelper;
import by.mmet.cafeshki.DataBase.DBUserPresenter;


public class AdressActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, View.OnClickListener {
    private Toolbar toolbar;
    private final static int PLACE_AUTOCOMPLETE_REQUEST_CODE = 12;
    EditText city;
    EditText street;
    EditText house;
    EditText flat;
    Button btnNextActivity;
    TextInputLayout textInputLayout;
    public static final String EXTRA_ADRESS = "myadress";


    private String address;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adress);


        //--------------------------Toolbar-------------------------
        toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        toolbar.setTitle("Ваш адрес");
        toolbar.setNavigationIcon(R.mipmap.ic_arrow_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        //       callPlaceAutocompleteActivityIntent();
        /*
        //--------------------------Toolbar-------------------------


        PlaceAutocompleteFragment places_street = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment_street);
        places_street.setHint("ваш адрес");
        places_street.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {

                Toast.makeText(getApplicationContext(), place.getName(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(Status status) {
                Toast.makeText(getApplicationContext(), status.toString(), Toast.LENGTH_SHORT).show();
            }

        });
        AutocompleteFilter typeFilter1 = new AutocompleteFilter.Builder()
                .setCountry("BY")
                .setTypeFilter(AutocompleteFilter.TYPE_FILTER_GEOCODE)
                .build();
        places_street.setFilter(typeFilter1);
*/
        btnNextActivity = (Button) findViewById(R.id.btnNextActivity);
        city = (EditText) findViewById(R.id.textacity);
        street = (EditText) findViewById(R.id.textacity1);
        house = (EditText) findViewById(R.id.textacity2);
        flat = (EditText) findViewById(R.id.textacity3);
        textInputLayout = (TextInputLayout) findViewById(R.id.TextInputLayout);
        //  String acity = getIntent().getStringExtra(EXTRA_ADRESS);
        String aCity = getIntent().getStringExtra(Constants.EXTRA_CITY_KEY);
        city.setText(aCity);
        String aStreet = getIntent().getStringExtra(Constants.EXTRA_STREET_KEY);
        street.setText(aStreet);
        String aHouse = getIntent().getStringExtra(Constants.EXTRA_HOUSE_KEY);
        house.setText(aHouse);
        address = city.getText() + ", " + street.getText() + ", " + house.getText() + ", " + flat.getText();;
    }

 /*   private void callPlaceAutocompleteActivityIntent() {
        try {
            Intent intent =
                    new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                            .build(this);
            startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
//PLACE_AUTOCOMPLETE_REQUEST_CODE is integer for request code
        } catch (GooglePlayServicesRepairableException |
                GooglePlayServicesNotAvailableException e) {
            // TODO: Handle the error.        }

        }}
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                latlong = place.getLatLng();
                Toast.makeText(getApplicationContext(), place.getName(), Toast.LENGTH_SHORT).show();
      //         txtlocation.setText(place.getAddress().toString());
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
            } else if (requestCode == RESULT_CANCELED) {

            }
        }
    }*/

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnNextActivity:
                DBUserPresenter dbUserPresenter = new DBUserPresenter(this);
                dbUserPresenter.put(null, null, address);
                Log.d("AddressActivity", "Запись добавлена");

                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(btnNextActivity.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                if (city.getText().length() == 0 || street.getText().length() == 0) {
                    Snackbar.make(v, "введите ваш адрес", Snackbar.LENGTH_SHORT).show();
                } else if (house.getText().length() == 0) {
                    Snackbar.make(v, "введите номер дома", Snackbar.LENGTH_SHORT).show();
                } else if (flat.getText().length() == 0) {
                    Snackbar.make(v, "введите номер квартиры", Snackbar.LENGTH_SHORT).show();
                } else {
                    Intent intent = new Intent(AdressActivity.this, RestaurantsActivity.class);
                    startActivity(intent);
                }
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}



