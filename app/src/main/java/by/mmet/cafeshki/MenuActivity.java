package by.mmet.cafeshki;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Adapter;


import java.util.ArrayList;
import java.util.List;


import by.mmet.cafeshki.Menu.FragmentMenu;
import by.mmet.cafeshki.Menu.FragPagerAdapter;



/**
 * Created by HP on 21.05.2017.
 */

public class MenuActivity extends AppCompatActivity {
  //  List<MenuModel> menuModels;
    RecyclerView mRV;
    private Toolbar toolbar;
    public static final String RESTAURANT_NAME = "restaurant_name";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.mipmap.ic_arrow_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        toolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimaryTransparent));
        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        this.addPages(viewPager);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        Intent intent = getIntent();
        final  String restaurantName = intent.getStringExtra(RESTAURANT_NAME);
        CollapsingToolbarLayout collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.main_collapsing);
        collapsingToolbarLayout.setTitle(restaurantName);

    }

    //ADD ALL PAGES
    private void addPages(ViewPager pager) {
        FragPagerAdapter adapter = new FragPagerAdapter(getSupportFragmentManager());
        ArrayList<Fragment> arrayList = new ArrayList<Fragment>();

        arrayList.add(new FragmentMenu());
        arrayList.add(new FragmentMenu());
        arrayList.add(new FragmentMenu());

        for(Fragment i: arrayList){
            adapter.addPage(i);
        }
      //  adapter.addPage(new MenuFragment());
       /* adapter.addPage(new PizzaFragment());
        adapter.addPage(new SushiFragment());
        adapter.addPage(new BurgersFragment());*/
        //ADD PAGER TO PAGE
        pager.setAdapter(adapter);
    }

}

