package by.mmet.cafeshki;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.util.Log;
import android.view.View;

import by.mmet.cafeshki.DataBase.DBHelper;
import by.mmet.cafeshki.DataBase.DBUserPresenter;

public class MainActivity extends AppCompatActivity {

    private DBHelper dbHelper;
    private SQLiteDatabase database;

    //  public final static String EXTRA_MESSAGE = "EXTRA_MESSAGE";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dbHelper = new DBHelper(this);
        database = dbHelper.getWritableDatabase();
        /*Cursor cursor = database.query(DBHelper.TABLE_USER, null, null, null, null, null, null);
        if(cursor.moveToLast()){

            int address = cursor.getColumnIndex(DBHelper.KEY_ADDRESS);
            Log.d("MainActivity", "DB Address: " + cursor.getString(address));
        }
        cursor.close();*/
        DBUserPresenter dbUserPresenter = new DBUserPresenter(this);
        Log.d("MainActivity","DB Address: " + dbUserPresenter.getAddress());
    }

    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.btn1_mainpage:
                intent = new Intent(MainActivity.this, MapsActivity.class);
                startActivity(intent);
                break;
            case R.id.btn2_mainpage:
                intent = new Intent(MainActivity.this, CitiesActivity.class);
                startActivity(intent);
                break;
        }

    }
}
