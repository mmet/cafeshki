package by.mmet.cafeshki;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class CitiesActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private final static int PLACE_AUTOCOMPLETE_REQUEST_CODE = 12;
    private LatLngBounds mLatLngBounds;
    LatLng latlong;
    Intent intent;
    private String mCityName;
    private final static String TAG = "CitiesActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cities);
        //--------------------------Toolbar-------------------------
        toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        toolbar.setTitle("Выберите город");
        toolbar.setNavigationIcon(R.mipmap.ic_arrow_back);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        //--------------------------Toolbar-------------------------

        ListView listView = (ListView) findViewById(R.id.cities);

        String[] cities = getResources().getStringArray(R.array.cities);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.custom_list_item, cities);
        listView.setAdapter(adapter);
        citychoice(listView);
    }

    private void citychoice(final ListView listView) {
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                intent = new Intent(CitiesActivity.this, AdressActivity.class);
                intent.putExtra(Constants.EXTRA_CITY_KEY, parent.getAdapter().getItem(position).toString());
                mCityName = parent.getAdapter().getItem(position).toString();
                callPlaceAutocompleteActivityIntent();
            }
        });

    }

    private void callPlaceAutocompleteActivityIntent() {
        try {
            AutocompleteFilter typeFilter = new AutocompleteFilter.Builder().
                    setTypeFilter(AutocompleteFilter.TYPE_FILTER_ADDRESS).
                    build();

            Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN).
                            setBoundsBias(mLatLngBounds).
                            setFilter(typeFilter).zzeZ(mCityName + ", ").
                            build(this);

            startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
        } catch (GooglePlayServicesRepairableException |
                GooglePlayServicesNotAvailableException e) {
            Log.e(TAG, "GooglePlayServicesRepairableException or GooglePlayServicesNotAvailableException");

        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.e(TAG, "onActivityResult");
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                String errorMessage = "";
                Place place = PlaceAutocomplete.getPlace(this, data);
                latlong = place.getLatLng();
                //--------------------------------------------------------
                Geocoder geocoder = new Geocoder(this, Locale.getDefault());
                List<Address> addresses = null;
                try {
                    addresses = geocoder.getFromLocation(
                            latlong.latitude,
                            latlong.longitude,
                            // In this sample, get just a single address.
                            1);
                } catch (IOException ioException) {
                    // Catch network or other I/O problems.
                    errorMessage = getString(R.string.service_not_available);
                    Log.e(TAG, errorMessage, ioException);
                } catch (IllegalArgumentException illegalArgumentException) {
                    // Catch invalid latitude or longitude values.
                    errorMessage = getString(R.string.invalid_lat_long_used);
                    Log.e(TAG, errorMessage + ". " +
                            "Latitude = " + latlong.latitude +
                            ", Longitude = " +
                            latlong.longitude, illegalArgumentException);
                }

                // Handle case where no address was found.
                if (addresses == null || addresses.size() == 0) {
                    if (errorMessage.isEmpty()) {
                        errorMessage = getString(R.string.no_address_found);
                        Log.e(TAG, errorMessage);
                    }

                } else {
                    Address address = addresses.get(0);

                    Log.w(TAG, "Address: " + address);
                    Log.w(TAG, "Address: " + address.getLocality());
                    Log.w(TAG, "Address: " + address.getThoroughfare());
                    Log.w(TAG, "Address: " + address.getSubThoroughfare());


                    intent.putExtra(Constants.EXTRA_STREET_KEY, address.getThoroughfare());
                    intent.putExtra(Constants.EXTRA_HOUSE_KEY, address.getSubThoroughfare());

                    startActivity(intent);
                }
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
            } else if (requestCode == RESULT_CANCELED) {

            }
        }
    }
}
