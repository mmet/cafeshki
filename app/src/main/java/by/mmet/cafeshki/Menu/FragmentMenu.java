package by.mmet.cafeshki.Menu;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/*import com.proba.proba.MenuRecycler.MenuAdapter;
import com.proba.proba.MenuRecycler.MenuModel;*/
import by.mmet.cafeshki.R;

import java.util.ArrayList;

/**
 * Created by HP on 23.05.2017.
 */

public class FragmentMenu extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_menu_recycler, null);
        //RECYCLERVIEW
        RecyclerView rv = (RecyclerView) v.findViewById(R.id.fragment_menu_recycler);
        rv.setLayoutManager(new LinearLayoutManager(this.getActivity()));
        rv.setAdapter(new MenuAdapter(this.getActivity(), getMenuBurgers()));
        return v;
    }

    private ArrayList<MenuModel> getMenuBurgers() {
        //COLLECTION OS BURGER MODELS
        ArrayList<MenuModel> models = new ArrayList<>();
        //SINGLE BURGER
        MenuModel menuModel = new MenuModel(R.drawable.pizza_test, "Бургер", "лук,сыр,соус,булка", "25р");
        //ADD ITR TO COLLECTION
        models.add(menuModel);

        menuModel = new MenuModel(R.drawable.pizza_test, "Бургер", "лук,сыр,соус,булка", "25р");
        models.add(menuModel);
        menuModel = new MenuModel(R.drawable.pizza_test, "Бургер", "лук,сыр,соус,булка", "25р");
        models.add(menuModel);
        menuModel = new MenuModel(R.drawable.pizza_test, "Бургер", "лук,сыр,соус,булка", "25р");
        models.add(menuModel);
        menuModel = new MenuModel(R.drawable.pizza_test, "Бургер", "лук,сыр,соус,булка", "25р");
        models.add(menuModel);
        menuModel = new MenuModel(R.drawable.pizza_test, "Бургер", "лук,сыр,соус,булка", "25р");
        models.add(menuModel);
        menuModel = new MenuModel(R.drawable.pizza_test, "Бургер", "лук,сыр,соус,булка", "25р");
        models.add(menuModel);
        menuModel = new MenuModel(R.drawable.pizza_test, "Бургер", "лук,сыр,соус,булка", "25р");
        models.add(menuModel);
        menuModel = new MenuModel(R.drawable.pizza_test, "Бургер", "лук,сыр,соус,булка", "25р");
        models.add(menuModel);
        menuModel = new MenuModel(R.drawable.pizza_test, "Бургер", "лук,сыр,соус,булка", "25р");
        models.add(menuModel);
        menuModel = new MenuModel(R.drawable.pizza_test, "Бургер", "лук,сыр,соус,булка", "25р");
        models.add(menuModel);


        return models;
    }

    //SET TITTLE FOR THE FRAGMENT

    @Override
    public String toString() {
        return ("Бургеры");
    }
}
