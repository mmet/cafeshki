package by.mmet.cafeshki.Menu;

import android.media.Image;

/**
 * Created by HP on 21.05.2017.
 */

public class MenuModel {
    int foodImage;
    String foodName;
    String recept;
    String foodPrice;
    public MenuModel(int foodImage,String foodName,String recept, String foodPrice){
        this.foodImage=foodImage;
        this.foodName=foodName;
        this.recept=recept;
        this.foodPrice=foodPrice;
    }
    public int getFoodImage(){return foodImage;}
    public String getFoodName(){return foodName;}
    public String getRecept(){return recept;}
    public String getFoodPrice(){return foodPrice;}

    public void setFoodImage(int foodImage) {
        this.foodImage = foodImage;
    }

    public void setFoodPrice(String foodPrice) {
        this.foodPrice = foodPrice;
    }

    public void setRecept(String recept) {
        this.recept = recept;
    }

    public void setFoodName(String foodName) {
        this.foodName = foodName;
    }
}

