package by.mmet.cafeshki.Menu;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;



import java.util.List;

import by.mmet.cafeshki.R;

/**
 * Created by HP on 21.05.2017.
 */

public class MenuAdapter extends RecyclerView.Adapter<MenuViewHolder> {
    Context o;
    List<MenuModel> menuModels;

    public MenuAdapter(Context o, List<MenuModel> menuModels) {
        this.o = o;
        this.menuModels = menuModels;
    }

    @Override
    public MenuViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.menu_card, parent, false);
        MenuViewHolder mVH = new MenuViewHolder(v);
        return mVH;
    }

    @Override
    public void onBindViewHolder(MenuViewHolder holder, int position) {
        holder.foodImage.setImageResource(menuModels.get(position).foodImage);
        holder.foodName.setText(menuModels.get(position).foodName);
        holder.recept.setText(menuModels.get(position).recept);
        holder.foodPrice.setText(menuModels.get(position).foodPrice);
    }

    @Override
    public int getItemCount() {
        return menuModels.size();
    }


}