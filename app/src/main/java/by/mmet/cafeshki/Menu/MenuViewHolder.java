package by.mmet.cafeshki.Menu;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import by.mmet.cafeshki.R;

/**
 * Created by Максим on 12.06.2017.
 */

public class MenuViewHolder extends RecyclerView.ViewHolder {
    ImageView foodImage;
    TextView foodName;
    TextView recept;
    TextView foodPrice;

    public   MenuViewHolder(View itemView) {
        super(itemView);
        foodImage = (ImageView) itemView.findViewById(R.id.foodImage);
        foodName = (TextView) itemView.findViewById(R.id.foodName);
        recept = (TextView) itemView.findViewById(R.id.recept);
        foodPrice = (TextView) itemView.findViewById(R.id.foodPrice);
    }

}
