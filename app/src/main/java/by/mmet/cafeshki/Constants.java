package by.mmet.cafeshki;

/**
 * Created by HP on 10.03.2017.
 */
public final class Constants {
        public static final String EXTRA_CITY_KEY= "a";
        public static final String EXTRA_STREET_KEY = "b";
        public static final String EXTRA_HOUSE_KEY = "c";
        public static final int SUCCESS_RESULT = 0;
        public static final int FAILURE_RESULT = 1;
        public static final String PACKAGE_NAME =
                "com.proba.proba";
        public static final String RECEIVER = PACKAGE_NAME + ".RECEIVER";
        public static final String RESULT_DATA_KEY = PACKAGE_NAME +
                ".RESULT_DATA_KEY";
        public static final String LOCATION_DATA_EXTRA = PACKAGE_NAME +
                ".LOCATION_DATA_EXTRA";
}
