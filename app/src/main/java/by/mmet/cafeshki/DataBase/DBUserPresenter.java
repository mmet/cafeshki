package by.mmet.cafeshki.DataBase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.media.Image;
import android.util.Log;

/**
 * Created by HP on 23.06.2017.
 */

public class DBUserPresenter {
    private int mId;
    private String mName;
    private String mPhone;
    private String mAddress;

    private DBHelper dbHelper;
    private SQLiteDatabase database;
    private ContentValues contentValues;
    private static final String TAG = "DB_USER";



    public DBUserPresenter(Context context) {
        dbHelper = new DBHelper(context);
        database = dbHelper.getWritableDatabase();
        contentValues = new ContentValues();
    }


    public void put(String name, String phone, String address) {
        this.mName = name;
        this.mPhone = phone;
        this.mAddress = address;
        contentValues.put(DBHelper.KEY_NAME,name);
        contentValues.put(DBHelper.KEY_PHONE,phone);
        contentValues.put(DBHelper.KEY_ADDRESS,address);
        Cursor cursor = database.query(DBHelper.TABLE_USER,null,null,null,null,null,null);
        if(cursor.getCount()==0){
            database.insert(DBHelper.TABLE_USER,null,contentValues);
            Log.e(TAG,"Данные добавились");
        }
        else{
            database.update(DBHelper.TABLE_USER,contentValues,null,null);
            Log.e(TAG,"Данные обновились");
            Log.e(TAG,"COUNT: " + cursor.getCount());
        }


    }
    public String getAddress(){
        Cursor cursor = database.query(DBHelper.TABLE_USER, null, null, null, null, null, null);
        if(cursor.moveToLast()){

            int address = cursor.getColumnIndex(DBHelper.KEY_ADDRESS);
            mAddress = cursor.getString(address);
        }
        cursor.close();
        return mAddress;
    }
public String getName(){
    Cursor cursor = database.query(DBHelper.TABLE_USER, null,null,null,null,null,null);
    if ((cursor.moveToLast())){
        int name = cursor.getColumnIndex(DBHelper.KEY_NAME);
        mName = cursor.getString(name);
    }
    cursor.close();
    return mName;
}
    public String getPhone(){
        Cursor cursor = database.query(DBHelper.TABLE_USER,null,null,null,null,null,null);
        if ((cursor.moveToLast())){
            int phone = cursor.getColumnIndex(DBHelper.KEY_PHONE);
            mPhone = cursor.getString(phone);
        }
        cursor.close();
        return mPhone;

}
}
