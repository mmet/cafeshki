package by.mmet.cafeshki.RestaurantRecycler;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;



import java.util.List;

import by.mmet.cafeshki.MenuActivity;

/**
 * Created by HP on 25.05.2017.
 */

public class RestaurantAdapter extends RecyclerView.Adapter<RestaurantViewHolder> {
    Context R;
    List<RestaurantModel> mRestaurantModels;

    public RestaurantAdapter(List<RestaurantModel> restaurantModels) {
        this.mRestaurantModels = restaurantModels;
    }

    @Override
    public RestaurantViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(by.mmet.cafeshki.R.layout.item_card, parent, false);
        RestaurantViewHolder mVH = new RestaurantViewHolder(v);
        return (mVH);
    }

    @Override
    public void onBindViewHolder(final RestaurantViewHolder holder, int position) {
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Context context = view.getContext();
                Intent intent = new Intent(context, MenuActivity.class);
                //intent.putExtra(MenuActivity.RESTAURANT_NAME, holder.restaurantName.getText());
                context.startActivity(intent);

            }
        });
        holder.restaurantName.setText(mRestaurantModels.get(position).getRestaurantName());
        holder.image.setImageResource(mRestaurantModels.get(position).getImgId());
        holder.food.setText(mRestaurantModels.get(position).getFood());
        holder.time.setText(String.valueOf(mRestaurantModels.get(position).getTime()));
        holder.price.setText(String.valueOf(mRestaurantModels.get(position).getPrice()));
        holder.rating.setText(String.valueOf(mRestaurantModels.get(position).getRating()));
        holder.votes.setText(mRestaurantModels.get(position).getVotes());
        holder.delivery.setText(mRestaurantModels.get(position).getDelivery());
        if(mRestaurantModels.get(position).isPoints())
        {
            holder.points.setVisibility(View.VISIBLE);
        }else{
            holder.points.setVisibility(View.GONE);
        }
       // holder.points.setText(mRestaurantModels.get(position).getPoints());
    }

    @Override
    public int getItemCount() {
        return mRestaurantModels.size();
    }
}
