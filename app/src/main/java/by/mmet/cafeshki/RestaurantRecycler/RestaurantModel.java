package by.mmet.cafeshki.RestaurantRecycler;

/**
 * Created by HP on 25.05.2017.
 */

public class RestaurantModel {
    private String restaurantName;
    private int imgId;
    private String food;
    private int time;
    private int price;
    private double rating;
    private String votes;
    private String delivery;
    private boolean points;

    public RestaurantModel(String restaurantName, int imgId, String food, int time, int price, double rating, int votes, String delivery, boolean points) {
        this.restaurantName = restaurantName;
        this.imgId = imgId;
        this.food = food;
        this.time = time;
        this.price = price;
        this.rating = rating;
        this.votes = "(" + Integer.toString(votes) + ")";
        this.delivery = delivery;
        this.points = points;
    }

    public String getRestaurantName() {
        return restaurantName;
    }



    public int getImgId() {
        return imgId;
    }



    public String getFood() {
        return food;
    }



    public int getTime() {
        return time;
    }



    public int getPrice() {
        return price;
    }



    public double getRating() {
        return rating;
    }



    public String getVotes() {
        return votes;
    }



    public String getDelivery() {
        return delivery;
    }



    public boolean isPoints() {
        return points;
    }


}
