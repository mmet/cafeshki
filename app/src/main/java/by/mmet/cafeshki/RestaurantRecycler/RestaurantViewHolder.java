package by.mmet.cafeshki.RestaurantRecycler;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import by.mmet.cafeshki.R;

/**
 * Created by HP on 25.05.2017.
 */

public class RestaurantViewHolder extends RecyclerView.ViewHolder {
    TextView restaurantName;
    ImageView image;
    TextView food;
    TextView time;
    TextView price;
    TextView rating;
    TextView votes;
    TextView delivery;
    TextView points;
    public final View mView;

    RestaurantViewHolder(View itemView) {
        super(itemView);
        mView = itemView;
        restaurantName = (TextView) itemView.findViewById(R.id.restaurantName);
        image = (ImageView) itemView.findViewById(R.id.image);
        food = (TextView) itemView.findViewById(R.id.food);
        time = (TextView) itemView.findViewById(R.id.time);
        price = (TextView) itemView.findViewById(R.id.price);
        rating = (TextView) itemView.findViewById(R.id.rating);
        votes = (TextView) itemView.findViewById(R.id.votes);
        delivery = (TextView) itemView.findViewById(R.id.delivery);
        points = (TextView) itemView.findViewById(R.id.points_true_or_false);
    }
}
