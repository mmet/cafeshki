package by.mmet.cafeshki;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import by.mmet.cafeshki.RestaurantRecycler.RestaurantAdapter;
import by.mmet.cafeshki.RestaurantRecycler.RestaurantModel;

import java.util.ArrayList;
import java.util.List;

public class RestaurantsActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    List<RestaurantModel> mRestaurantModels;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurants);
        toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        toolbar.setTitle("Все рестораны");
        toolbar.setNavigationIcon(R.mipmap.ic_arrow_back);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        recyclerView = (RecyclerView) findViewById(R.id.recyclerRestaurant);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        initData();
        initAdapter();
    }

    private void initData() {
        mRestaurantModels = new ArrayList<>();
        mRestaurantModels.add(new RestaurantModel("ПАРМЕЗАН пиццерия", R.drawable.pizza_test, "Итальянская", 60, 20, 5, 6545, "Бесплатная доставка", true));
        mRestaurantModels.add(new RestaurantModel("Burger Pizza", R.drawable.pizza_test, "Пицца/Бургеры", 90, 15, 4, 4341, "Бесплатная доставка", false));
        mRestaurantModels.add(new RestaurantModel("Габрово ресторан", R.drawable.pizza_test, "Белорусская", 40, 10, 3.5, 41, "Платная доставка", false));
        mRestaurantModels.add(new RestaurantModel("BurgerRestaurant", R.drawable.pizza_test, "Пицца/Суши/Бургер", 25, 10, 4.8, 459841, "tnm", true));
    }

    private void initAdapter() {
        RestaurantAdapter adapter = new RestaurantAdapter(mRestaurantModels);
        recyclerView.setAdapter(adapter);

    }
}
