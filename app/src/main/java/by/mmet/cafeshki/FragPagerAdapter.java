package by.mmet.cafeshki;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

/**
 * Created by HP on 23.05.2017.
 */

public class FragPagerAdapter extends FragmentPagerAdapter {
    ArrayList<Fragment> pages = new ArrayList<>();
    public FragPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return pages.get(position);
    }

    @Override
    public int getCount() {
        return pages.size();
    }
    //ADD A PAGE
    public void addPage(Fragment f){
        pages.add(f);
    }
    //SET TITTLE FOR TAB

    @Override
    public CharSequence getPageTitle(int position) {
        return pages.get(position).toString();
    }
}
